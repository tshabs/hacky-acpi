README
======

The horrible things we have to do to get an Asus ROG GX501G minimally usable on linux.

First of all using Kernel 4.19 seemed to add stability (especially for power management)

Suspend
-------

Until I can figure out why the fuck the script doesn't run in /etc/acpi/ there's a horrible set of crons:

```
# m h  dom mon dow   command
* * * * * ~/hacky-acpi/lid.sh
* * * * * sleep 10; ~/hacky-acpi/lid.sh
* * * * * sleep 20; ~/hacky-acpi/lid.sh
* * * * * sleep 30; ~/hacky-acpi/lid.sh
* * * * * sleep 40; ~/hacky-acpi/lid.sh
* * * * * sleep 50; ~/hacky-acpi/lid.sh
```

you probably need to
```
chmod +x ~/hacky-acpi/lid.sh
```


Backlight
---------

For now the solution was just to map Ctrl+F7 (rather than using FN key) to:

```
xbacklight -dec 10
```

and Ctrl+F8 to:

```
xbacklight -inc 10
```
