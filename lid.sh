#!/bin/bash
if [ -n "`grep close /proc/acpi/button/lid/LID/state`" ]; then
    systemctl suspend
    logger -s "ACPI lid closed";
elif [ -n "`grep open /proc/acpi/button/lid/LID/state`" ]; then
#    logger -s "ACPI lid opened";
    :
else
    logger -s "Couldn't detect Lid state"
fi
